exec podman run --rm -it \
    --device /dev/kvm \
    --volume $PWD:/src:rw \
    --security-opt label=disable \
    --workdir /src \
    nix-dev
