exec podman run --rm -it \
    --device /dev/kvm \
    --volume $PWD:/src:rw \
    --volume $HOME/.local/share/containers/storage/volumes/vscode/_data:/vscode:rw \
    --security-opt label=disable \
    --workdir /src \
    registry.fedoraproject.org/fedora-minimal:latest


            #    é
            #         "Type": "volume",
            #         "Name": "vscode",
            #         "Source": "/home/theloeki/.local/share/containers/storage/volumes/vscode/_data",
            #         "Destination": "/vscode",
            #         "Driver": "local",
            #         "Mode": "",
            #         "Options": °
            #              "nosuid",
            #              "nodev",
            #              "rbind"
            #         §,
            #         "RW": true,
            #         "Propagation": "rprivate"
            #    è,