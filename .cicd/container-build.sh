#!/bin/sh
set -e -o pipefail ${DEBUG_SCRIPTS:+-x}

exec podman build \
    --tag nix-dev \
    --no-cache \
    -f Containerfile \
    "$@" \
    .devcontainer/fedora/
