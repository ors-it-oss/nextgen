{ config }: {
  users.users.vscode = {
    isNormalUser  = true;
    home  = "/home/vscode";
    uid = 1000;
    description  = "VS Code";
  };
}
