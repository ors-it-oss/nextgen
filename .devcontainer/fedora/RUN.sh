#!/bin/sh
set -e ${DEBUG_RUN:+-x} -o pipefail

micro_root=/mnt/sysroot/
nix_installer_pkgs="curl-minimal shadow-utils sudo tar xz"


# Installs a micro baseline in a sysroot dir
install_microroot(){
    nix_pkgs="coreutils-single glibc-minimal-langpack"
    # util_pkgs="coreutils-single findutils"
    vscode_pkgs="libstdc++"
    microdnf -y install \
        --installroot "$micro_root" \
        --use-host-config \
        --no-docs \
        --setopt install_weak_deps=false \
        $nix_pkgs $util_pkgs $vscode_pkgs $nix_installer_pkgs
}


# Setup Nix pkg mgr
setup_nix(){
    # broken on bubblewrap because /dev/fd/45 no in there somehow
    #sh <(curl -L https://nixos.org/nix/install) --daemon
    curl -L https://nixos.org/nix/install | sh -s -- --daemon

    echo 'extra-experimental-features = flakes nix-command' >>/etc/nix/nix.conf

    source /etc/profile.d/nix.sh

    ### Get up to date
    nix-channel --update
    nix-env --upgrade

    ### Install that devenv thing
    # nix-env --install --attr cachix --file https://cachix.org/api/v1/install
    # (USER=${USER:-root} cachix use devenv)

    # nix-env --install --file https://github.com/cachix/devenv/tarball/latest


    ### Install minimum deps for devcontainers et al
    nix-env --install --attr \
        nixpkgs.findutils \
        nixpkgs.gitMinimal \
        nixpkgs.gnugrep \
        nixpkgs.gnupg \
        nixpkgs.gnused \
        nixpkgs.gnutar \
        nixpkgs.gzip \
        nixpkgs.man \
        nixpkgs.nil \
        nixpkgs.nixpkgs-fmt
        # nixpkgs.nix-index \
        # nixpkgs.coreutils \


    ### Cleanup the store
    nix-collect-garbage --delete-old
    nix-store --gc
    nix-store --optimise
}


# Post operations
finalize(){
    # Remove pkgs only needed for install
    microdnf -y remove\
        --use-host-config \
        --installroot "$micro_root" \
        $nix_installer_pkgs

    # Cleanup 
    rm -rf \
        /var/{log,tmp}/* \
        /root/.cache/*
    find /etc -name '*.backup-before-nix' -exec rm {} \; 2>&1
}


# bwrap_nix(){
#     microdnf -y install \
#         --no-docs \
#         --setopt install_weak_deps=false \
#         bubblewrap
#
#     # --proc /proc \
#     # --share-net \
#     # TODO: b0rks on trying to chown stuff because bubblewrap apparently hardcodes no_new_privs
#     bwrap \
#         --cap-add CAP_CHOWN \
#         --cap-add CAP_SETGID \
#         --cap-add CAP_SETUID \
#         --unshare-all \
#         --share-net \
#         --dev /dev \
#         --tmpfs /tmp \
#         --bind "$microroot" / \
#         --ro-bind /etc/resolv.conf /etc/resolv.conf \
#         --ro-bind ./run_nix_micro.sh /run_nix_micro.sh \
#         /run_nix_micro.sh
# }


# home_prep(){
#     useradd -d /home/vscode -m -s /bin/sh -u 1000 -U -c 'VScode user' vscode
#     chmod 1777 /home
#     setfacl --physical --default --modify 'u::rwx,g::rx,o::---' /home
# }


###########

if [[ -n "$@" ]]; then
    func=$1
    shift
    $func "$@"
fi
