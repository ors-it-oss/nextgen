#!/bin/sh
set -e ${DEBUG_SCRIPTS:+-x} -o pipefail

unshared(){
    source ./RUN.sh

    # Create a container
    container=$(buildah from scratch)

    # Mount the container filesystem
    microroot=$(buildah mount $container)

    install_microroot 

    buildah run --volume ./RUN.sh:/sbin/RUN.sh $container -- /sbin/RUN.sh setup_nix

    buildah run --volume ./RUN.sh:/sbin/RUN.sh $container -- /sbin/RUN.sh finalize

    # Save the container to an image
    buildah commit --format docker $container nix-buildahd

    # Cleanup
    buildah unmount $container


    # # Push the image to the Docker daemon’s storage
    # buildah push nginx:latest docker-daemon:nginx:latest
}


###########

if [[ -z "$@" ]]; then
    buildah unshare "$0" unshared
else
    "$@"
fi
