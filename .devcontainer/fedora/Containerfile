# Stage 1: Build a micro root
FROM registry.fedoraproject.org/fedora-minimal:latest AS builder
ARG DEBUG_RUN
RUN \
  --mount=type=bind,source=RUN.sh,target=/sbin/RUN.sh \
  --mount=type=cache,target=/var/cache/libdnf5,sharing=locked \
  --mount=type=cache,target=/var/lib/dnf,sharing=locked \
  exec /sbin/RUN.sh install_microroot


# Stage 2: Stage into microroot and add Nix
FROM scratch AS nix
COPY --from=builder /mnt/sysroot /
ARG DEBUG_RUN
RUN --mount=type=bind,source=RUN.sh,target=/sbin/RUN.sh \
  exec /sbin/RUN.sh setup_nix


# Stage 3: Stage back into sysroot & cleanup
FROM registry.fedoraproject.org/fedora-minimal:latest AS postop
COPY --from=nix / /mnt/sysroot
ARG DEBUG_RUN
RUN --mount=type=bind,source=RUN.sh,target=/sbin/RUN.sh \
  --mount=type=cache,target=/var/cache/libdnf5,sharing=locked \
  --mount=type=cache,target=/var/lib/dnf,sharing=locked \
  exec /sbin/RUN.sh finalize


# Final stage: micro base + Nix final sysroot
FROM scratch
LABEL \
  org.opencontainers.image.title="Nix dev container" \
  org.opencontainers.image.description="Container for Nix"
COPY --from=postop /mnt/sysroot /
# ENV PS1='[$EUID:$GROUPS@\h \W]\$ '
ENV LANG="C.UTF-8"
CMD ["/bin/sh", "-l"]
