https://github.com/CrowdStrike/Dockerfiles/blob/main/Dockerfile.ubi_micro
https://bastide.org/2021/09/02/playing-with-buildah-and-ubi-micro-part-1/
https://quarkus.io/guides/quarkus-runtime-base-image

https://ksingh7.medium.com/how-to-build-lightweight-and-secure-container-images-using-rhel-ubi-at-zero-cost-28fbb93bd6dd

https://www.redhat.com/sysadmin/tiny-containers
https://git.almalinux.org/srbala/ks2rootfs/src/branch/main/Dockerfile

https://github.com/rpm-software-management/microdnf/issues/86

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/building_running_and_managing_containers/assembly_adding-software-to-a-ubi-container_building-running-and-managing-containers
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/building_running_and_managing_containers/index#proc_using-the-ubi-micro-images_assembly_adding-software-to-a-ubi-container
https://catalog.redhat.com/software/containers/ubi9-micro/61832b36dd607bfc82e66399?container-tabs=overview


https://github.com/silveraid/docker-centos-micro/blob/master/Dockerfile

https://www.redhat.com/sysadmin/tiny-containers



```
(exec bwrap --ro-bind /usr /usr \
            --ro-bind /bin /bin \
            --ro-bind /lib /lib \
            --ro-bind /lib64 /lib64 \
            --ro-bind /etc/resolv.conf /etc/resolv.conf \
            --dir /tmp \
            --dir /var \
            --dir $HOME \
            --symlink ../tmp var/tmp \
            --unshare-all \
            --share-net \
            --die-with-parent \
            --dir /run/user/$(id -u) \
            --setenv XDG_RUNTIME_DIR "/run/user/`id -u`" \
            --file 11 /etc/passwd \
            --file 12 /etc/group \
            /bin/bash) \
        11< <(getent passwd $UID 65534) \
        12< <(getent group $(id -g) 65534)
```