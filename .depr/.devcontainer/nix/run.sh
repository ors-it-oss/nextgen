#!/bin/sh
set -eo pipefail

echo 'extra-experimental-features = flakes nix-command' >>/etc/nix/nix.conf
echo 'vscode:x:1000:1000:VSCode user:/home/vscode:/bin/sh' >>/etc/passwd
echo 'vscode:x:1000:vscode' >>/etc/group
mkdir -pm 755 /home/vscode
chown -R vscode:vscode /home/vscode

nix-channel --update

nix-env --install --attr nixpkgs.gnupg nixpkgs.gnused 
# nixpkgs.shadow nixpkgs.su 

# openat(AT_FDCWD, "/etc/passwd", O_RDWR|O_NOCTTY|O_NONBLOCK|O_NOFOLLOW) = -1 ELOOP (Too many levels of symbolic links)
# write(2, "useradd: cannot open /etc/passwd"..., 33useradd: cannot open /etc/passwd
# useradd -d /home/vscode -m -s /bin/sh -u 1000 -U -c 'VScode user' vscode


# nix-store --gc
