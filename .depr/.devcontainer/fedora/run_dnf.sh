#!/bin/sh
set -eo pipefail

set -x

microdnf -y upgrade 
microdnf -y install acl git-core gnupg shadow-utils sudo tar xz 
# microdnf clean all

useradd -d /home/vscode -m -s /bin/sh -u 1000 -U -c 'VScode user' vscode
chmod 1777 /home
setfacl --physical --default --modify 'u::rwx,g::rx,o::---' /home
