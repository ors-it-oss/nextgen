#!/bin/sh
set -eo pipefail

set -x

curl -L https://nixos.org/nix/install | sh -s -- --daemon

echo 'extra-experimental-features = flakes nix-command' >>/etc/nix/nix.conf

source /etc/profile.d/nix.sh

nix-channel --update

nix-env --install --attr cachix --file https://cachix.org/api/v1/install
(USER=${USER:-root} cachix use devenv)

nix-env --install --attr nixpkgs.nix-index

nix-store --gc
