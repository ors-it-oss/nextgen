#!/bin/sh
set -e errexit -o pipefail

set -x

# broken on bubblewrap because /dev/fd/45 no in there somehow
#sh <(curl -L https://nixos.org/nix/install) --daemon
curl -L https://nixos.org/nix/install | sh -s -- --daemon

echo 'extra-experimental-features = flakes nix-command' >>/etc/nix/nix.conf

source /etc/profile.d/nix.sh

### Get up to date
nix-channel --update
nix-env --upgrade

### Install that devenv thing
# nix-env --install --attr cachix --file https://cachix.org/api/v1/install
# (USER=${USER:-root} cachix use devenv)

# nix-env --install --file https://github.com/cachix/devenv/tarball/latest
    # nixpkgs.nix-index \

### Install minimum deps for devcontainers et al
nix-env --install --attr \
    nixpkgs.gitMinimal \
    nixpkgs.gnupg \
    nixpkgs.gnused \
    nixpkgs.man 

### Cleanup the store
nix-store --gc
nix-store --optimise
