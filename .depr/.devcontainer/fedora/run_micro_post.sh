#!/bin/sh
set -e -o pipefail
set -x

microroot=/mnt/sysroot/
nix_installer_pkgs="curl-minimal shadow-utils sudo tar xz"

microdnf -y remove\
    --use-host-config \
    --installroot "$microroot" \
    $nix_installer_pkgs


# Cleanup 
rm -rf \
    /var/{log,tmp}/* \
    /root/.cache/* \
find / -name *.backup-before-nix -exec rm {}\;
