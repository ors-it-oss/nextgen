#!/bin/sh
set -e -o pipefail
set -x

microroot=/mnt/sysroot/

nix_pkgs="coreutils-single findutils glibc-minimal-langpack"
nix_installer_pkgs="curl-minimal shadow-utils sudo tar xz"

# Install a basic filesystem and minimal set of packages, and nginx
microdnf -y install \
    --installroot "$microroot" \
    --use-host-config \
    --no-docs \
    --setopt install_weak_deps=false \
    $nix_pkgs $nix_installer_pkgs


# microdnf -y install \
#     --no-docs \
#     --setopt install_weak_deps=false \
#     bubblewrap
#
# --proc /proc \
# --share-net \
# TODO: b0rks on trying to chown stuff because bubblewrap apparently hardcodes no_new_privs
# bwrap \
#     --cap-add CAP_CHOWN \
#     --cap-add CAP_SETGID \
#     --cap-add CAP_SETUID \
#     --unshare-all \
#     --share-net \
#     --dev /dev \
#     --tmpfs /tmp \
#     --bind "$microroot" / \
#     --ro-bind /etc/resolv.conf /etc/resolv.conf \
#     --ro-bind ./run_nix_micro.sh /run_nix_micro.sh \
#     /run_nix_micro.sh

# useradd -d /home/vscode -m -s /bin/sh -u 1000 -U -c 'VScode user' vscode
# chmod 1777 /home
# setfacl --physical --default --modify 'u::rwx,g::rx,o::---' /home
